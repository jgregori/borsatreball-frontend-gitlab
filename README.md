# borsatreball-frontend

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

# Proyecto Bolsa de trabajo de Batoi

![](/public/img-home.png)

# Descripción

¿Para que sirve esta aplicación?

La bolsa de trabajo de Batoi permite que antiguos alumnos que han cursado uno o más grados y alumnos que están cursando algún grado que ofrece el centro CIP FP BATOI, puedan registrarse para poder aplicar a las ofertas de trabajo que lanzan las empresas, las cuales han de registrarse antes de poder comenzar a lanzar ofertas.

Para comprobar la veracidad de los ciclos que ha cursado un antiguo alumno o que está cursando un alumno, un responsable de ciclo ha de comprobar que dicho alumno haya cursado o esté cursando el o los ciclos que ha indicado.

# Tabla de contenidos

1. [Tecnologías utilizadas](#tecnologías-utilizadas)
2. [Puesta en marcha](#puesta-en-marcha)
3. [Entornos](#entornos)
4. [Guía de Contribución](#guía-de-contribución)
5. [Lista de Contribuidores](#lista-de-contribuidores)
6. [Licencia](#licencia)

## Tecnologías utilizadas

### Backend

- PHP 8.2
- Laravel 10

## Frontend

- vite
- JS
- Vue 3 en su versión de Options API y Composition Setup
- pinia
- vue-router
- font-awesome
- axios
- sass
- yup
- vee-validate
- bootstrap-icons

## Puesta en marcha

Clonar el repo
```bash
git clone https://github.com/BorsaDeTreball-ProyectoFinal/BorsaTreball-Frontend.git
```

Situarse en el repositorio e instalar las dependencias
```bash
cd BorsaTreball-Frontend
npm i
```

## Entornos

_Enlaces a entornos de producción, staging, desarrollo (si existen)_

## Guía de contribución

_Cualquier contribución al proyecto deberá seguir las siguientes (enlace a CONTRIBUTING.md)_

## Lista de contribuidores

- Ruben
- Jorge Hernández
- Alvaro Lucas Pérez
- Jorge Gregori Tandazo
- Fran Gregori Tandazo

## Licencia

_Este proyecto se desarrolla y distribuye de acuerdo a los términos de la [Licencia Mit](LICENSE) incluida._
