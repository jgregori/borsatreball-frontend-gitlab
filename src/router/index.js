import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import AccessView from '@/views/AccessView.vue'
import AboutView from '@/views/AboutView.vue'
import LoginView from '@/views/LoginView.vue'
import NewCompanyFormView from '@/views/companies/NewCompanyFormView.vue'
import NewOfferView from '@/views/offers/NewOfferView.vue'
import NewResponsibleFormView from '@/views/responsibles/NewResponsibleFormView.vue'
import NewStudentFormView from '@/views/students/NewStudentFormView.vue'
import ModStudentFormView from '@/views/students/ModStudentFormView.vue'
import StudentsInOfferView from '@/views/offers/StudentsInOfferView.vue'
import EnterpriseListView from '@/views/companies/EnterpriseListView.vue'
import SeeAllStudentsView from '@/views/students/SeeAllStudentsView.vue'
import RegisterView from '@/views/RegisterView.vue'
import OffersView from '@/views/offers/OffersView.vue'
import CompanyMenu from '@/components/CompanyMenu.vue'
import StudentMenu from '@/components/StudentMenu.vue'
import AdminMenu from '@/components/AdminMenu.vue'

import NotFoundComponentView from '@/views/NotFoundComponentView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: '/',
      component: AccessView,
      meta: {
        requireAuth: false
      }
    },
    {
      path: '/home',
      name: 'home',
      component: HomeView,
      meta: {
        requireAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: LoginView,
      meta: {
        requireAuth: false
      }
    },
    {
      path: '/register',
      name: 'register',
      component: RegisterView,
      meta: {
        requireAuth: false
      }
    },
    {
      path: '/register/company',
      name: 'register-company',
      component: NewCompanyFormView,
      meta: {
        requireAuth: false
      }
    },
    {
      path: '/register/responsible',
      name: 'register-responsible',
      component: NewResponsibleFormView,
      meta: {
        requireAuth: false
      }
    },
    {
      path: '/register/student',
      name: 'register-student',
      component: NewStudentFormView,
      meta: {
        requireAuth: false
      }
    },
    {
      path: '/create-offer',
      name: 'create-offer',
      component: NewOfferView,
      meta: {
        requireAuth: true,
        requireRoles: ['admin', 'company']
      }
    },
    {
      path: '/enterprise-list',
      name: 'enterprise-list',
      component: EnterpriseListView,
      meta: {
        requireAuth: true,
        requireRoles: ['admin']
      }
    },
    {
      path: '/students-list',
      name: 'students-list',
      component: SeeAllStudentsView
    },
    {
      path: '/edit-student/:studentId',
      name: 'edit-student',
      component: ModStudentFormView,
      props: true,
      meta: {
        requireAuth: true,
        requireRoles: ['admin', 'student']
      }
    },
    {
      path: '/view-students-in-offer',
      name: 'view-students-in-offer',
      component: StudentsInOfferView,
      meta: {
        requireAuth: true,
        requireRoles: ['admin', 'company']
      }
    },
    {
      path: '/offers',
      name: 'offers',
      component: OffersView,
      meta: {
        requireAuth: true,
        requireRoles: ['admin', 'student', 'company']
      }
    },
    {
      path: '/about',
      name: 'about',
      component: AboutView,
      meta: {
        requireAuth: false
      }
    },
    {
      path: '/:catchAll(.*)',
      component: NotFoundComponentView
    }
  ]
})

router.beforeEach((to, from, next) => {

  if (to.meta.requireAuth && !sessionStorage.token) {
    console.log('Debes iniciar sesión')
    next('/')
    return
  }

  if (to.meta.requireRoles && !to.meta.requireRoles.includes(sessionStorage.role)) {
    console.log('No tienes los permisos necesarios')
    return
  }
  next()
})

export default router
