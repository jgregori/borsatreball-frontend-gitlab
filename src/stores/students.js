import { defineStore } from 'pinia'
import Repository from '@/repositories/Repository.class'

export const useStudentsStore = defineStore('students', {
    state() {
        return {
            students: [],
            repository: new Repository('students')
        }
    },
    actions: {
        async loadStudents() {
            try {
                this.students = await this.repository.getAll()
            } catch (e) {
                throw new Error(e)
            }
        },
        async remove(student) {
            try {
                await this.repository.remove(student)
            } catch (e) {
                throw new Error(e.message)
            }
        }
    },

})