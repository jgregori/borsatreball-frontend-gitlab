import { ref } from 'vue'
import { defineStore } from 'pinia'
import Repository from '@/repositories/Repository.class'

export const useCompaniesStore = defineStore('companies', () => {
  const items = ref([])

  async function loadCompanies() {
    const repository = new Repository('companies')
    try {
      const companies = await repository.getAll()
      items.value = companies
    } catch (error) {
      throw new Error(error)
    }
  }

  /* El método remove no debe de eliminar la compañia por completo, ha de guardar las ofertas que ha lanzado */
  async function remove(company) {
    const repository = new Repository('companies')
    try {
      await repository.remove(company)
    } catch (error) {
      throw new Error(error)
    }
  }

  return { items, loadCompanies, remove }
})
