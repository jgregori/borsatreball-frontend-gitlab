import { ref } from 'vue'
import { defineStore } from 'pinia'
import Repository from '@/repositories/Repository.class'

export const useCyclesStore = defineStore('cycles', () => {
  const items = ref([])

  async function loadCycles() {
    const repository = new Repository('cycles')
    try {
      let cycles = await repository.getAll()
      cycles = cycles.data.sort((cycle1, cycle2) => cycle1.departamento - cycle2.departamento)
      items.value = cycles
    } catch (error) {
      throw new Error(error)
    }
  }

  return { items, loadCycles }
})