import { ref } from 'vue'
import { defineStore } from 'pinia'
import Repository from '@/repositories/Repository.class'

export const useProfessionalFamiliesStore = defineStore('professionalFamilies', () => {
  const items = ref([])

  async function loadFamilies() {
    const repository = new Repository('professionalFamilies')
    try {
      const families = await repository.getAll()
      items.value = families.data
    } catch (error) {
      throw new Error(error)
    }
  }

  function findById(id) {
    return items.value.find((item) => item.id == id)
  }

  return { items, loadFamilies, findById }
})
