import axios from 'axios'
const SERVER = import.meta.env.VITE_URL_API

const apiClient = axios.create({
  baseURL: `${SERVER}/api`,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + sessionStorage.token
  }
})

export default class Repository {
  constructor(table) {
    this.table = table
  }

  async getAll() {
    try {
      const response = await apiClient.get(`/${this.table}`)
      return response.data
    } catch (error) {
      console.log(error)
      throw new Error(error)
    }
  }

  async getPage(page) {
    try {
      const response = await apiClient.get(`/${this.table}?page=${page}`)
      return response.data.data
    } catch (error) {
      throw new Error(error)
    }
  }

  async getPaginate(page) {
    try {
      const response = await apiClient.get(`/${this.table}?/page${page}`)
      return response.data
    } catch (error) {
      throw new Error(error)
    }
  }

  async add(item) {
    try {
      return await apiClient.post(`/${this.table}`, item)
    } catch (error) {
      console.log(error)
      throw new Error(error)
    }
  }

  async update(item) {
    console.log(item)
    try {
      return await apiClient.patch(`/${this.table}/${item.id}`, item)
    } catch (error) {
      console.log(error)
      throw new Error(error)
    }
  }

  async remove(item) {
    try {
      const response = await apiClient.delete(`/${this.table}/${item.id}`);
      return response.data
    } catch (error) {
      throw new Error(error);
    }
  }

  async findById(id) {
    try {
      const response = await apiClient.get(`/${this.table}/${id}`)
      return response.data
    } catch (error) {
      console.log(error)
      throw new Error(error)
    }
  }

  async getJobOffersByCompany(id) {
    try {
      const response = await apiClient.get(`/${this.table}?company_id=${id}`)
      console.log(response)
      return response.data
    } catch (error) {
      console.log(error)
      throw new Error(error)
    }
  }

  async getCiclesByStudents(id) {
    try {
      const response = await apiClient.get(`/${this.table}?student_id=${id}`)
      console.log(response)
      return response.data
    } catch (error) {
      console.log(error)
      throw new Error(error)
    }
  }
}
