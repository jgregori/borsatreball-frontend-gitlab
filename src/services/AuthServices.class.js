import axios from 'axios'
import { ref } from 'vue'
const SERVER = import.meta.env.VITE_URL_API

const apiClient = axios.create({
  baseURL: `${SERVER}/api`,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + sessionStorage.token
  }
})

export default class AuthServices {
  // No funcionará ya que está api no está diseñada de esa forma, no poseen password ni tmp ningún access_token
  // Lo correcto es que la API tuviera la password y su access_token
  constructor(table) {
    this.table = table
    this.jwt = ref('')
    this.error = ref('')
  }

  getJwt() {
    return this.jwt
  }

  getError() {
    return this.error
  }

  login(email, password) {
    try {
      const response = apiClient.post(`/${this.table}`, {
        email: email,
        password: password
      })
      if (response.status === 200) {
        // Inicio de sesión exitoso, manejar la respuesta de la API
        console.log('Inicio de sesión exitoso:', response.data);
      } else {
        // La solicitud no fue exitosa, manejar el error
        console.error('Error en la solicitud:', response.statusText);
      }
      return response
    } catch (error) {
      return error
    }
  }

  loginWithGoogle() {
    try {
      const response = axios.get(`${SERVER}/auth/google`)
      console.log(response)
      // Redirige al usuario al inicio de sesión de Google
      window.location.href = response.data.redirect
    } catch (error) {
      console.error(error)
    }
  }
}
