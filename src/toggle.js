export function toggle() {
  const toggle = document.getElementById('container')
  const body = document.querySelector('body')
  const sunIcon = toggle.querySelector('.fa-sun')
  const moonIcon = toggle.querySelector('.fa-moon')

  toggle.onclick = function () {
    toggle.classList.toggle('active')
    body.classList.toggle('active')

    if (toggle.classList.contains('active')) {
      sunIcon.style.display = 'none'
      moonIcon.style.display = 'block'
    } else {
      sunIcon.style.display = 'block'
      moonIcon.style.display = 'none'
    }
  }
}
